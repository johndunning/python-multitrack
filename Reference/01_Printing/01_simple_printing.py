name = 'Jon Doe'
age = 21
sex = 'M'
height = 1.85
weight = 182

# -- simple print
print(name)
print(age)
print(sex)
print(height)
print(weight)
print()

# -- combine into a single print
print(name, age, sex, height, weight)
print()

# -- add messages
print('name:', name)
print('age:', age)
print('sex:', sex)
print('height:', height)
print('weight:', weight)
print()
