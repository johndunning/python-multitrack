# $Note
# * extract the snippet
# * highlight the code
# * output it as an image
# -Note

# $Code
from PIL import Image, ImageDraw, ImageFont

from pygments import highlight
from pygments.lexers import PythonLexer
from pygments.formatters import ImageFormatter
import io
# -Code

# $Code
code = '''
from pygments import highlight
from pygments.lexers import PythonLexer
from pygments.formatters import ImageFormatter
import imgkit

# brew install wkhtmltopdf


code = 'print "Hello World"'
gif = highlight(code, PythonLexer(), ImageFormatter())

imageFile = open("images/code.png", "wb")
# write to file
imageFile.write(gif)
imageFile.close()
'''
# -Code

# $Note
# Highlight the Code
# $Code
imgbytes = highlight(code, PythonLexer(), ImageFormatter())
# -Code
# -Note

# $Note
# Create the image
# $Code
img = Image.open(io.BytesIO(imgbytes))
# -Code
# -Note

# $Note
# Display the image
# $Code
img.show()
# -Code
# _Note
