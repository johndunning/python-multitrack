from PIL import Image, ImageDraw, ImageFont

from pygments import highlight
from pygments.lexers import PythonLexer
from pygments.formatters import ImageFormatter
import io

# brew install wkhtmltopdf


code = '''
from pygments import highlight
from pygments.lexers import PythonLexer
from pygments.formatters import ImageFormatter
import imgkit

# brew install wkhtmltopdf


code = 'print "Hello World"'
gif = highlight(code, PythonLexer(), ImageFormatter())

imageFile = open("images/code.png", "wb")
# write to file
imageFile.write(gif)
imageFile.close()
'''

imgbytes = highlight(code, PythonLexer(), ImageFormatter())

img = Image.open(io.BytesIO(imgbytes))
img.show()
