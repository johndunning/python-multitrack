from PIL import Image, ImageDraw, ImageFont

from imagewriter.s4_container import S4_Container
from imagewriter.s4_text_line import S4_Line


class S4_Image(S4_Container):

    def __init__(self, background_image: Image, base_style):
        S4_Container.__init__(self, background_image, base_style)

        self.image = None
        self.image_width = 0
        self.image_height = 0
        self.x = 0
        self.y = 0

        self.width = 0
        self.height = 0
        self.maxsize = None

    def _initialise_image(self, image: Image, overriding_style=None):
        self.override_style(overriding_style)

        self.image = image
        self.x = 0
        self.y = 0

        self.scale_image()

        self.image_width, self.image_height = image.size
        self.maxsize = self.background_image_width, self.background_image_height

        self.width = self.image_width
        self.height = self.image_height

        self.delta_x = int(self.image_width / 10)
        self.delta_y = int(self.image_height / 10)

    def scale_image(self):
        percentage = self.style.get('scale')
        if percentage:
            self.maxsize = (int(self.background_image_width * percentage / 100), int(self.background_image_height * percentage / 100))

        self.image.thumbnail(self.maxsize, Image.ANTIALIAS)

    def _position_image(self):
        self.position_container()

    def draw(self, image: Image, overriding_style=None):
        self._initialise_image(image, overriding_style)
        self._position_image()

        self.background_image.paste(self.image, (self.x, self.y))

        print(self.x, self.y, self.width, self.height)

        super().draw()

    def __str__(self):

        return str({
            'image': self.image,
            'width': self.width,
            'height': self.height
        })


if __name__ == '__main__':
    image = Image.open('images/background.png')
    logo = Image.open('images/logo.png')

    base_style = {'position': ['bottom', 'right'], 'scale': 20, 'indent': (0, 0, 10, 10)}
    s4_image = S4_Image(image, base_style)
    s4_image.draw(logo)

    image.save('images/image_logo_test.png')

    image.show()

