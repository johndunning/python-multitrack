"""
Indexing can be used to return one character in the current string
Slicing can be used to return a substring of the current string

indexing:  string[idx] - 0, length -1
slicing:   string[start:stop:step]
"""
#01234567890
"Hello World"   # 11 characters

str = "Hello World"
print(str)
print(str[0])
print(str[10])

print("Hello World"[0:3])
print("Hello World"[4:7])
print("Hello World"[-1:])
print("Hello World"[-2:])
print("Hello World"[9:])
print("Hello World"[-5:])
print("Hello World"[-5:-2])
print("Hello World"[-1:-4:-1])
print("Hello World"[::-1])

print("Hello World"[:2])