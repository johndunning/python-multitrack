"""
Int, or integer, is a whole number, positive or negative, without decimals, of unlimited length.
"""

age = 21
x = 234897890235709234981274920348920401243240978029348239854235087235235720735097230572039570
y = -3249827349279847

print(age)
print(x)
print(y)

print(type(age))
print(type(x))
print(type(y))
