
"""
Decimal Notation
100 10 1
--------
       9    = 9
     9 9    = 99
 1  0  0    = 100
  3  7 1    = 371

Binary Notation
4 2 1
-----
    0       = 0
    1       = 1
  1 0       = 2
  1 1       = 3
1 0 0       = 4

Converting Decimal to Binary, Octal, and Hexadecimal
10 1
----
 7 1 = 71

64 32 16 8 4 2 1
----------------
 1  0  0 0 1 1 1 = 71

64 8 1
------
 1 0 7 = 71

256 16 1
--------
     4 7 = 71
"""

binary_str = "1000111"
octal_str = "107"
hex_str = "47"

dec1 = int(binary_str, 2)
dec2 = int(octal_str, 8)
dec3 = int(hex_str, 16)

print(dec1)
print(dec2)
print(dec3)

dec_value = 71

print("Decimal: {:d}, Binary: {:b}, Octal: {:o}, Hex: {:x}"
      .format(dec_value, dec_value, dec_value, dec_value))

binary_value = str.format("{:b}",dec_value)
print(binary_value)