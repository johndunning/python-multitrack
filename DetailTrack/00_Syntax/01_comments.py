# this is a comment
# it can be used on
# multiple lines

print('Hello World')    # it can also be used at the end of a line of code

# comments can be used to describe code
# or to prevent code from executing

# print('Goodbye World')

'''
a multi line comment
can also be enclosed in 3 single quotes
'''

"""
a multi line comment
can also be enclosed in 3 double quotes
"""

